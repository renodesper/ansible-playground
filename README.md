## Start VM's
```
$ vagrant up
```

## Start Provisioning
```
$ vagrant provision
```

*Once the VM's are up and running, install tmuxinator and run mux. This will connect to the provisioned 3 Vagrant VM's.*

### SSH into VM's (in case if you don't use tmuxinator)
```
$ vagrant ssh master
$ vagrant ssh devops1
$ vagrant ssh devops2
```

## Generate master key and copy to client
```
vagrant@master:~ $ ssh-keygen
vagrant@master:~ $ ssh-copy-id vagrant@192.168.10.101
vagrant@master:~ $ ssh-copy-id vagrant@192.168.10.102
```

*The password for the vagrant user on all machines is vagrant.*

## Usage
```
vagrant@master:~ $ ansible dev -a 'uname -a'
vagrant@master:~ $ ansible dev -a 'uptime'
```

## Stop VM's
```
$ vagrant halt
```
